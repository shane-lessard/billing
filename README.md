An application for building and tracking invoices. 

Planned upgrades:
    
    * Unregistered invoices - anon user - add details for one time user - alias email (non-unique, no log-in email)
    * Revamp of Invoice Builder (using vue.js)
    * Split invoice rows - Regular rows and modifier rows (discount rows, tax rows)
    * Statistics page
    * Upgrade to Laravel 5.3
    * Alternative file format choices (Excel - Word)
    
Current stable features:

    * Track/manage clients
    * Build/Delete/Export (PDF) Invoices
    * User detail management