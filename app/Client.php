<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Client extends Model
{

    static $rules = array(
        'name' => 'required',
        'address' => 'required',
    );

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'clients';

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'address',
        'phone',
        'business',
        'notes',
        'user_id'
    ];

    /**
     * user relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Invoices relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function invoices()
    {
        return $this->hasMany('App\Invoice');
    }

    /**
     * InvoiceRows relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function invoiceRows()
    {
        return $this->hasManyThrough('App\InvoiceRow', 'App\Invoice');
    }

    /**
     * Database timestamps for creation and edit
     *
     * @var bool
     */
    public $timestamps = true;


    use SoftDeletes;

}
