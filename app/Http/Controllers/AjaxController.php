<?php
namespace App\Http\Controllers;

use Auth;
use App\Client;

class AjaxController extends Controller

{
    /**
     * A controller for ajax calls
     */

    /**
     * Get a list of Clients in <option> tags
     *
     * @return string
     */
    function getClients()
    {
        //After saving clients, call this view

        $clients = Auth::user()->clients()->orderBy('name')->get();

        $list = "<option disabled selected value='null'>- Select a Client -</option>";

        foreach ($clients as $client) {
            $list .= "<option value='" . $client->id . "'>" . $client->name . "&nbsp;&nbsp;&nbsp;&nbsp;&lt;<em>" . $client->email . "</em>&gt;</option>";
        }
        return $list;
    }

    /**
     * Get a JSON object full of ItemRow results
     *
     * @return JSON object
     */
    function getItems()
    {
        $json = json_encode(Auth::user()->invoiceRows()->orderBy('id', 'desc')->limit(20)->get());

        return $json;
    }

    /**
     * Get a JSON object of invoices for a client
     *
     * @param $id client_id
     * @return JSON object
     */
    function getInvoices($id)
    {

        $client = Client::find($id);

        if ($client->user_id == Auth::user()->id){

            $invoices = $client->invoices()->orderBy('paid')->get();

            $json = json_encode($invoices);

            return $json;
        }

        return "[]";

    }

}
