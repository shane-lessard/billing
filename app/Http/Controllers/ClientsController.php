<?php
namespace App\Http\Controllers;

use App\Client;
use Auth;
use DB;
use Validator;
use Illuminate\Http\Request;
use Input;
use Redirect;

class ClientsController extends Controller
{

    /**
     * TODO transform address into Address Line 1/2 + City + Code + Country, as User account
     */

    /**
     * ClientsController constructor
     * Use auth middleware
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Get client management page
     *
     * @return View
     */
    public function index()
    {

        $title = env('SITE_TITLE') . " | Client Management";

        $data = ['title' => $title];

        if (Auth::user()->clients()) {
            $data = [
                'title' => $title,
                'clients' => Auth::user()
                    ->clients()
                    ->orderBy('name')
                    ->paginate(15)
            ];
        }

        return view('pages.clients', $data);
    }

    public function store(Request $request)
    {

        $validation = Validator::make($request->all(), Client::$rules);

        if ($validation->fails()) {

            $request->flash();
            return Redirect::back()->withInput()->withErrors($validation->messages());

        }

        $inputs = Input::all();

        //Add user_id to inputs array
        $inputs['user_id'] = Auth::user()->id;

        Client::create($inputs);

        return Redirect::back();

    }

    public function destroy(Request $request)
    {

        $input = $request;

        if (Client::find($input['id'])->user_id == Auth::user()->id) {
            Client::find($input['id'])->invoiceRows()->delete();
            Client::find($input['id'])->invoices()->delete();
            Client::find($input['id'])->delete();
            return Redirect::back();
        }
    }

    public function update(Request $request, $id)
    {
        $validation = Validator::make($request->all(), Client::$rules);

        if ($validation->fails()) {

            $request->flash();
            return Redirect::back()->withInput()->withErrors($validation->messages());

        }

        $input = $request;

        if (Client::find($id)->user_id == Auth::user()->id) {
            Client::find($id)->update(array(
                'email' => $input['email'],
                'phone' => $input['phone'],
                'address' => $input['address'],
                'business' => $input['business'],
                'notes' => $input['notes']
            ));
            return Redirect::back();
        }

    }

}

