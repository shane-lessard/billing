<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Invoice;
use App\InvoiceRow;
use Redirect;
use App;
use Session;
use Validator;

class InvoicesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth', [
            'except' => [
                'getView',
                'getPdf',
            ]

        ]);
    }

    /**
     * Validate and store new invoice
     *
     * @return Redirect to new invoice
     */
    public function postStore(Request $request)
    {

        $input = $request->all();

        $input["user_id"] = Auth::user()->id;

        $validation = Validator::make($input, Invoice::$rules, Invoice::$messages);

        if ($validation->fails()) {

            $request->flash();
            return Redirect::back()
                ->withInput()
                ->withErrors($validation->messages());

        }

        //get the amount of rows as a number
        $rows = intval($request->rows);

        //invoiceRow validation requires an amount of rows
        $validation = Validator::make($input, InvoiceRow::rules($rows), InvoiceRow::messages($rows));

        if ($validation->fails()) {

            $request->flash();
            return Redirect::back()
                ->withInput()
                ->withErrors($validation->messages());
        }

        $invoice = new Invoice;

        $invoice->fill($input);

        for ($i = 0; $i < $rows; $i++) {

            $row[$i] = new InvoiceRow(array(
                'item' => $request->item[$i],
                'description' => $request->description[$i],
                'cost' => $request->cost[$i],
                'quantity' => $request->quantity[$i],
                'total' => $request->itemTotal[$i]
            ));

        }

        $invoice->save();
        $invoice->InvoiceRows()->saveMany($row);

        return redirect('invoice/view/' . $invoice->id);


    }

    /**
     * Show invoice or redirect home if $id = null
     *
     * @param null $id
     * @return View
     */
    public function getView($id = null)
    {

        if (Invoice::find($id) == null) {

            return redirect('/');

        }

        $invoice = Invoice::find($id);
        $rows = $invoice->invoiceRows;
        $biller = $invoice->user;
        $client = $invoice->client;

        if (($invoice->private == 1 && null == Auth::user()) || ($invoice->private == 1 && $invoice->user_id != Auth::user()->id)) {
            //return view for private invoice message
            return redirect('/');
        }

        return view('invoice', array(
            'invoice' => $invoice,
            'rows' => $rows,
            'biller' => $biller,
            'client' => $client,
        ));
    }

    /**
     * Generate PDF document for invoice
     *
     * @param $id
     * @return PDF
     */
    public function getPdf($id)
    {

        if (Invoice::find($id) == null) {

            return redirect('/');

        }

        $invoice = Invoice::find($id);
        $rows = $invoice->invoiceRows;
        $biller = $invoice->user;
        $client = $invoice->client;

        if (($invoice->private == 1 && null == Auth::user()) || ($invoice->private == 1 && $invoice->user_id != Auth::user()->id)) {
            //return view for private invoice message
            return redirect('/');
        }

        $pdf = App::make('snappy.pdf.wrapper');

        $pdf->loadView('pdf', array(
            'invoice' => $invoice,
            'rows' => $rows,
            'biller' => $biller,
            'client' => $client,
        ));

        return $pdf->download($client->name . " - $" . $invoice->total . " - " . $invoice->created_at->format('Y-m-d') . '.pdf');

    }

    /**
     * @param $id
     * @return Reciept for payment
     */
    public function getReceipt($id)
    {
        $invoice = Invoice::find($id);

        if ($invoice == null || !Auth::check() || Auth::user()->id !== $invoice->user_id){
            return redirect('/');
        }

        if ($invoice->paid == null){
            return redirect('/');
        }

        $pdf = App::make('snappy.pdf.wrapper');

        $pdf->loadView('receipt', array(
            'invoice' => $invoice,
            'rows' => $invoice->invoiceRows,
            'biller' => $invoice->user,
            'client' => $invoice->client,
        ));

        $paid = date('Y/M/d',strtotime($invoice->paid));

        return $pdf->download($invoice->client->name . " - $" . $invoice->total . " - " . $paid);
    }

    /**paid
     * Mark invoice as
     *
     * @param $id
     * @return redirect back
     */
    public function getPay($id)
    {
        $invoice = Invoice::find($id);
        if ($invoice->user_id == Auth::user()->id) {
            $invoice->paid = date("Y-m-d H:i:s");
            $invoice->save();
        }

        return redirect()->back();
    }

    /**
     * Mark invoice as unpaid
     *
     * @param $id
     * @return redirect back
     */
    public function getUnpay($id)
    {
        $invoice = Invoice::find($id);
        if ($invoice->user_id == Auth::user()->id) {
            $invoice->paid = NULL;
            $invoice->save();
        }

        return redirect()->back();
    }

    /**
     * Mark invoice as private, and hide from other users/anonymous users
     *
     * @param $id
     * @return redirect back
     */
    public function getPrivate($id)
    {
        $invoice = Invoice::find($id);
        if ($invoice->user_id == Auth::user()->id) {
            $invoice->private = 1;
            $invoice->save();
        }
        return redirect()->back();
    }

    /**
     * Make invoice public
     *
     * @param $id
     * @return redirect back
     */
    public function getPublic($id)
    {
        $invoice = Invoice::find($id);
        if ($invoice->user_id == Auth::user()->id) {
            $invoice->private = 0;
            $invoice->save();
        }
        return redirect()->back();
    }

    public function getRemove($id)
    {
        $invoice = Invoice::find($id);
        if ($invoice->user_id == Auth::user()->id) {

            $invoice->invoiceRows()->delete();
            $invoice->delete();
        }
        return redirect()->back();
    }

    /**
     * Get list of paid invoices
     *
     * @return View
     */
    public function getPaid()
    {

        if (Auth::check()) {

            $data = array(
                'invoices' => Auth::user()->invoices()->whereNotNull('paid')->orderBy('id', 'desc')->paginate(15),
                'title' => 'Paid Invoices',
                'type' => 'paid',
            );

            return view('invoice-list', $data);

        }

    }

    /**
     * Get list of unpaid invoices
     *
     * @return View
     */
    public function getUnpaid()
    {

        if (Auth::check()) {

            $data = array(
                'invoices' => Auth::user()->invoices()->whereNull('paid')->orderBy('id', 'desc')->paginate(15),
                'title' => 'Unpaid Invoices',
                'type' => 'unpaid'
            );

            return view('invoice-list', $data);

        }

    }

}
