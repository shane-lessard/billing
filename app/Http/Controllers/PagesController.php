<?php namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Http\Middleware\Authenticate;


class PagesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Get homepage / invoice builder
     *
     * @return View
     */
    public function getIndex(Request $request)
    {

        $title = env('SITE_TITLE') . " | Home ";
        $due = date("Y-m-d", strtotime("+1 month"));

        $data = ['title' => $title, 'due' => $due];

        $input = $request->old();

        if (Auth::user()->clients()) {
            $data = [
                'title' => $title,
                'due' => $due,
                'clients' => Auth::user()->clients()->orderBy('name')->get(),
                'invoiceNumber' => Auth::user()->invoices()->orderBy('id', 'desc')->limit(1)->pluck('invoice_number') + 1,
                'rows' => !empty($input) ? $input['rows'] : 1,
            ];
        }


        return view('home', $data);
    }

    public function getHome()
    {
        return redirect('/');
    }

    /**
     * Get account/profile page for editing account information and checking statistics
     *
     * @return View
     */
    public function getAccount()
    {

        $title = env('SITE_TITLE') . " | Account and User Settings";
        $user = Auth::user();


        $data = array(
            'title' => $title,
            'user' => $user,
        );

        return view('pages.account', $data);
    }

}