<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use DB;
use App\User;
use Validator;
use Redirect;


class UsersController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Update account
     *
     * @param Request $requests
     * @return Redirector
     */
    public function postUpdate(Request $requests)
    {
        $request = $requests->all();

        $validation = Validator::make($request, User::$rulesNoPass);

        if ($validation->fails()) {

            return Redirect::back()->withInput()->withErrors($validation->messages());

        }

        if ($request['id'] = Auth::user()->id) {

            User::find(Auth::user()->id)->update($request);
        }

        return redirect('account');
    }

}
