<?php

/**
 * Built in Auth routes
 *
 * TODO convert to resource controller or implicit routes
 */

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

/**
 * Manage clients resource RESTfully
 */

Route::resource('clients', 'ClientsController');

/**
 * Users controller
 */

Route::post('user/update', 'UsersController@postUpdate');

/**
 * Invoice Controller
 *
 * TODO convert to resource controller or implicit routes
 */
Route::controller('invoice', 'InvoicesController');

/**
 * Ajax routes
 *
 * Return JSON objects
 */

Route::get('ajax/clients', 'AjaxController@getClients');

Route::get('ajax/items', 'AjaxController@getItems');

Route::get('ajax/invoices', 'AjaxController@getInvoices');

/**
 * Main/Home routes
 */

Route::get('/', 'PagesController@getIndex');

Route::get('home', function(){
    return redirect('/');
});

Route::get('account', 'PagesController@getAccount');