<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model
{


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'invoices';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'currency',
        'tax',
        'tax_method',
        'discount',
        'discount_method',
        'invoice_number',
        'total',
        'due',
        'notes',
        'user_id',
        'client_id'
    ];

    protected $dates = ['deleted_at'];

    /**
     * @var array of validation rules for invoice
     */
    static $rules = array(
        'currency' => 'required',
        'client_id' => 'required',
        'due' => 'required',
        'tax' => 'numeric',
        'discount' => 'numeric',
        'invoice_number' => 'numeric',
        'notes' => 'max:600',
        'total' => 'numeric|required',
    );

    static $messages = array(
        'currency.required' => 'The currency field is required',
        'client_id.required' => 'Please select a client',
        'due.required' => 'Please select a due date',
        'tax.numeric' => 'Tax must be a numeric value',
        'discount.numeric' => 'Discount must be a numeric value',
        'notes.max' => 'The notes field should be less than 600 characters',
        'total.numeric' => 'The total field must be a number',
        'total.required' => 'The total field is required',
    );

    /**
     * Relationship for invoiceRows
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function invoiceRows()
    {
        return $this->hasMany('App\InvoiceRow');
    }

    /**
     * Relationship for users
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Relationship for clients
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo('App\Client');
    }

    /**
     * Turn on timestamps
     *
     * @var bool
     */
    public $timestamps = true;
    use SoftDeletes;

}