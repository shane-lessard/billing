<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Input;


class InvoiceRow extends Model
{


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'invoice_rows';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'item',
        'description',
        'cost',
        'quantity',
        'total'
    ];

    /**
     * Validation rules
     *
     * @return array
     */
    static function rules($count)
    {
        $rules = array();

        //Loop through each row to validate each line separately
        for ($i = 0; $i < $count; $i++){

            $rules["item.$i"] = 'required|max:30';
            $rules["description.$i"] = 'required|max:90';
            $rules["itemTotal.$i"] = 'required|numeric';
            $rules["cost.$i"] = 'required|numeric';
            $rules["quantity.$i"] = 'required|numeric';

        }

        return $rules;
    }

    static function messages($count)
    {
        $messages = array();

        //Loop through each row to validate each line separately
        for ($i = 0; $i < $count; $i++){

            $row = $i + 1;

            $messages["item.$i.required"] = "Row $row needs a name";
            $messages["description.$i.required"] = "Row $row needs a description";
            $messages["itemTotal.$i.required"] = "Row $row needs a total";
            $messages["cost.$i.required"] = "Row $row needs a cost";
            $messages["quantity.$i.required"] = "Row $row needs a quantity";

            $messages["item.$i.max"] = "Row $row name must be under 30 characters";
            $messages["description.$i.max"] = "Row $row description must be under 90 characters";
            $messages["itemTotal.$i.numeric"] = "Row $row total must be a number";
            $messages["cost.$i.numeric"] = "Row $row cost must be a number";
            $messages["quantity.$i.numeric"] = "Row $row quantity must be a number";

        }

        return $messages;
    }

    /**
     * Invoice relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function invoice()
    {
        return $this->belongsTo('App\Invoice');
    }

    public $timestamps = false;

}