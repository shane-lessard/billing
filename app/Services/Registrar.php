<?php namespace App\Services;

use App\User;
use Validator;
use Illuminate\Contracts\Auth\Registrar as RegistrarContract;

class Registrar implements RegistrarContract
{

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validator(array $data)
    {
        return Validator::make($data, User::$rules);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    public function create(array $data)
    {

        $avatar = (!isset($data['avatar']))? 0:$data['avatar'];

        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'business' => $data['business'],
            'address_line_1' => $data['address_line_1'],
            'address_line_2' => $data['address_line_2'],
            'postal' => $data['postal'],
            'province' => $data['province'],
            'city' => $data['city'],
            'country' => $data['country'],
            'password' => bcrypt($data['password']),
            'avatar' => $avatar,
        ]);
    }

}
