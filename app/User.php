<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Validator;
use DB;
use Auth;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{

    use Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'business',
        'address_line_1',
        'address_line_2',
        'postal',
        'city',
        'province',
        'country',
    ];

    public static $rules = array(
        'name' => 'required',
        'email' => 'required|email',
        'password' => 'required|min:4|confirmed',
    );

    public static $rulesNoPass = array(
        'name' => 'required',
        'email' => 'required',
    );

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * Client relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function clients()
    {
        return $this->hasMany('App\Client');
    }

    /**
     * Invoices relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function invoices()
    {
        return $this->hasMany('App\Invoice');
    }

    /**
     * invoiceRows relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function invoiceRows()
    {
        return $this->hasManyThrough('App\InvoiceRow', 'App\Invoice');
    }

    /**
     * Check if user has invoices, paid or unpaid
     */
    static function hasInvoice()
    {
        return Auth::user()->invoices()->count() > 0 ? true : false;
    }

    static function hasPaid()
    {
        return Auth::user()->invoices()->whereNotNull('paid')->count() > 0 ? true : false;
    }

    static function hasUnpaid()
    {
        return Auth::user()->invoices()->whereNull('paid')->count() > 0 ? true : false;
    }

    /**
     * Statistic functions
     * TODO calculate functions for statistics on dashboard
     */

    /**
     * @return float this year's total invoiced amount
     */

    function currentCharged()
    {

    }

    /**
     * @return float last year's total invoiced amount
     */

    function lastCharged()
    {

    }

    /**
     * @return float total invoiced amount
     */
    function totalCharged()
    {

    }

    /**
     * @return float this year's total revenue
     */
    function currentRevenue()
    {

    }

    /**
     * @return float last year's total revenue
     */
    function lastRevenue()
    {

    }

    /**
     * @return float total revenue
     */
    function totalRevenue()
    {

    }

    /**
     * @return float this year's total expenses
     */
    function currentExpenses()
    {

    }

    /**
     * @return float last year's total expenses
     */
    function lastExpenses()
    {

    }

    /**
     * @return float total expenses
     */
    function totalExpenses()
    {

    }

    /**
     * @return float current year's profit (paid - expenses)
     */
    function currentProfit()
    {

    }

    /**
     * @return float last year's profit
     */
    function lastProfit()
    {

    }

    /**
     * @return float total profit
     */
    function totalProfit()
    {

    }

}
