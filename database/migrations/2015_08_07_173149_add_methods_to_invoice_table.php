<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMethodsToInvoiceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//add tax_method and discount_method to invoice table
        Schema::table('invoices', function(Blueprint $table)
        {
            $table->string('tax_method');
            $table->string('discount_method');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//Drop methods
        Schema::table('invoices', function(Blueprint $table)
        {
            $table->dropColumn('tax_method');
            $table->dropColumn('discount_method');
        });
	}

}
