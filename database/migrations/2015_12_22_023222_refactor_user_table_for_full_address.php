<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RefactorUserTableForFullAddress extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //rename address to address_line_1 and add line 2, postal, province and country

        Schema::table('users', function($table)
        {
            $table->renameColumn('address', 'address_line_1');
            $table->string('address_line_2');
            $table->string('postal');
            $table->string('city');
            $table->string('province');
            $table->string('country');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //rollback
        Schema::table('users', function($table)
        {
            $table->renameColumn('address_line_1', 'address');
            $table->dropColumn('address_line_2');
            $table->dropColumn('postal');
            $table->dropColumn('city');
            $table->dropColumn('province');
            $table->dropColumn('country');
        });
    }

}
