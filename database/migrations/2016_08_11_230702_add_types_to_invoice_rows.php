<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypesToInvoiceRows extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('invoice_rows', function(Blueprint $table)
        {
            $table->string('type')->default('item');
            $table->string('operator')->default('+');
            $table->string('modifier')->default('$');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
        Schema::table('invoice_rows', function(Blueprint $table)
        {
            $table->dropColumn('type');
            $table->dropColumn('operator');
            $table->dropColumn('modifier');
        });
	}

}
