$(document).ready(function() {


    /**
     * Invoice construction page
     */

    function setMethod(type, method) {

        /*
        Set method for tax or discount, % or $
         */

        $('span.' + type + '-method').toggleClass('active');
        $('input.' + type + '-method').val(method);

        calculateTotal();
    }

    $('span.discount-method').click(function(){
        if (!$(this).hasClass('active')) {
            if ($('input.discount-method').val() == "%") {
                setMethod('discount', '$');
            }

            else {
                setMethod('discount', '%');
            }
        }
    });

    $('span.tax-method').click(function(){
        if (!$(this).hasClass('active')) {

            {
                if ($('input.tax-method').val() == "%") {
                    setMethod('tax', '$');
                }

                else {
                    setMethod('tax', '%');
                }
            }
        }
    });

    //depending on old input, set initial active methods

    if($('input.tax-method').val() == "$"){
        $('span.tax-method').toggleClass('active');
    }

    if($('input.discount-method').val() == "%"){
        $('span.discount-method').toggleClass('active');
    }

    function calculateRows(){

        /*
        Calculate Item Row
         */

        var sum = 0;

        $('tr.item-row').each(function(){
            var cost = parseFloat($(this).find('td').find('.item-cost').val());
            var qty = parseFloat($(this).find('td').find('input.item-qty').val());
            var total = cost * qty;

            if (total > 0) {

                $(this).find('.item-total').val(total.toFixed(2));
                sum += total;

            }


        })
        return sum;
    }

    function calculateTotal(){

        /*
        Calculate Invoice Total
        */

        var sum = calculateRows();
        var tax = parseFloat($('input.tax').val());
        var taxMethod = $('input.tax-method').val();
        var discount = parseFloat($('input.discount').val());
        var discountMethod = $('input.discount-method').val();

        if (!isNaN(discount) && discount > 0) {
            if (discountMethod == '$') {
                sum -= discount;
            }

            else if (discountMethod == "%") {
                discount = 100 - discount;
                sum *= discount / 100;
            }

        }

        if (!isNaN(tax) && tax > 0) {

            if (taxMethod == "%") {

                sum += sum * tax / 100;
            }

            if (taxMethod == "$") {
                sum += tax;
            }

        }

            if (sum > 0 && !isNaN(sum)) {
                $('.invoice-total').val(sum.toFixed(2));
            }

    }

    function incrementQuantity(element){

        /*
        Increase quantity of line item
         */

        element = element.parents('.input-group').find('input.item-qty');

        element.val(parseFloat(element.val()) + 1);

        calculateTotal();


    }

    function decrementQuantity(element){

        /*
        Decrease quantity of line item
         */

        element = element.parents('.input-group').find('input.item-qty');

        element.val(parseFloat(element.val()) - 1);

        if (parseFloat(element.val()) < 0 && $('.item-row').length > 1) {

            element.parents('tr').remove();

            $('.rows').val(parseInt($('.rows').val()) - 1);

        }

        else if (parseFloat(element.val()) < 0 && $('.item-row').length === 1){

            element.val(0);

        }

        calculateTotal();

    }

    /*
     Adds a new row to the invoice
     */
    function addRow(){

        if ($('.item-row').length < 15) {
            var newRow = $('.item-row').eq(0).clone();

            newRow.find('input').val('');

            newRow.find('input.item-qty').val('1');

            newRow.appendTo('table.invoice-table');

            $('.rows').val(parseInt($('.rows').val()) + 1);

        }
    }

    $('button.add-row').click(function(){
        addRow();
    })


    $('body').on('click', '.glyphicon-chevron-up.item-qty', function(){

        var element = $(this);

        incrementQuantity(element);
    })

    $('body').on('click', '.glyphicon-chevron-down.item-qty', function(){

        var element = $(this);

        decrementQuantity(element);
    });

    $('body').on('change', '.invoice-app input', function(){
        calculateTotal();
    });

    $('body').on('keyup', function(){
        calculateTotal();
    });

    $('body').on('mouseclick', function(){
        calculateTotal();
    });

    //Confirm before deleting Clients

    $('.remove-client').click(function(){
       return confirm(
           "Removing this client will erase all client information, and each invoice assigned to this client. \n\nAre you sure you wish to continue?"
       );
    });

    //Confirm before deleting Invoices

    $('.remove-invoice').click(function(){
        return confirm(
            "Are you sure you want to delete this invoice? It can not be recovered."
        );
    });

    calculateTotal();

    function submitClient(){
        //Submit client with ajax as to not disrupt invoice creation form
        
        var data = {
            "_token" : $('#new-client input[name=_token]').val(),
            "name" : $('#new-client input[name=name]').val(),
            "email" : $('#new-client input[name=email]').val(),
            "phone" : $('#new-client input[name=phone]').val(),
            "address" : $('#new-client input[name=address]').val(),
            "business" : $('#new-client input[name=business]').val(),
            "notes" : $('#new-client input[name=notes]').val()
        };
        
        $.post("/clients", data, function(){
            // Reload client list select menu
            $('.client-list').load('/ajax/clients', function(){
                // close select menu
                $('.close-client-form').click();

                //alert user that client has been added
                $('.success-message').html('Client has been added!');
                $('.alert-success.hidden-element').show();
            });
        });

    }

    $('.index-new-client').click(function(event){
        //Register on click event for submitClient()
        event.preventDefault();

        submitClient();

    });

    /**
     * Ajax search for previous items
     *
     * Search past item names, and if selected, will populate item name, description and cost
     */

    // Get invoiceRows json object by calling ajax controller getItems method
    var invoiceRows = 0;

    function getInvoiceRows(){


        $.getJSON('/ajax/items', function (data) {

            invoiceRows = data;

        });

    }


    /**
     * Search through data on keyup to load a list of saved Rows, and allow user to click list item to add to invoice
     */
    $('html').on('keyup', '.item-name', function(e){

        if (invoiceRows == 0) {
            getInvoiceRows();
        }

        var code = e.keyCode || e.which;


        //get value of item input
        var field = $(this).val();
        //get data from ajax call
        var data = invoiceRows;

        //get .saved-item
        var saved = $(this).parents('tr').find('.saved-item');

        //get list item
        var list = saved.find('li');

        //If event does not match up, down or enter
        if(code != 38 && code != 40 && code != 13) {

            //start with clean slate
            list.remove();
            saved.hide();

            //array of items that have already been found, to avoid duplicates

            var foundItems = [];

            //Check if input is found in saved item row name
            for (var i = 0; i < data.length; i++) {

                //Check to see if item name has been found
                if ($.inArray(data[i].item, foundItems) == -1) {

                    //Check if matches item name - case insensitive
                    if (data[i].item.toLowerCase().indexOf(field.toLowerCase()) != -1) {
                        saved
                            .append('<li title="$' + data[i].cost + '"data-item="' + i + '">' + data[i].item + '</li>');

                        saved.show();
                    }

                    foundItems.push(data[i].item);
                }
            }
        }
        //selected from list
        var selected = $('.saved-item li.active').index('.saved-item li');
        var max = list.length;

        //if pressing down and .saved-item li exists, cycle through list items

        if (code == 40){
            list.removeClass('active');

            //increment if not last item
            if (selected == max - 1){
                selected = 0;
            }

            else {
                selected++;
            }
            console.log(selected);
            list.eq(selected).addClass('active');

        }
        //if pressing up and .saved-item li  exists, cycle through list items

        if (code == 38){
            list.removeClass('active');

            //increment if not last item
            if (selected == 0){
                selected = max - 1;
            }

            else {
                selected--;
            }
            console.log(selected);
            list.eq(selected).addClass('active');

        }

        //if pressing enter and .saved-item li.active exists, select item

        if (code == 13){

            var o = $('.saved-item li.active').data('item');

            list.parents('tr').find('.item-name').val(data[o].item);
            list.parents('tr').find('.item-description').val(data[o].description);
            list.parents('tr').find('.item-cost').val(data[o].cost);
            list.parents('.saved-item').hide();

            calculateTotal();
        }

    });

    //Pressing enter must return false on keydown, not keyup
    $('html').on('keydown', '.item-name', function(e){

        if (e.which == 13){
            return false;
        }

    });

    //click saved item to add it's values (name, description and cost) to item row
    $('body').on('mousedown', '.saved-item li', function(){
        //get data index
        var i = $(this).data('item');
        //get data object
        var data = invoiceRows;

        $(this).parents('tr').find('.item-name').val(data[i].item);
        $(this).parents('tr').find('.item-description').val(data[i].description);
        $(this).parents('tr').find('.item-cost').val(data[i].cost);
        $(this).parents('.saved-item').hide();

        calculateTotal();
    });

    //close item list when leaving input
    $('body').on('blur', 'input.item-name', function(){
        $('.saved-item').hide();
        $('.saved-item li').remove();
    });

    /**
     * Ajax to retrieve list of invoices for client
     */

    $('a.get-list').on('click', function(){

        //Get ID and Client Name
        var id = $(this).data('cid');
        var name = $(this).data('name');

        //Get fillable table form modal
        var table = $('#client-invoices').find('tbody');

        //Set client name on modal
        $('#client-invoices').find('h4').html(name);

        //clear table
        table.html('');

        //Get JSON file with relevant client invoices
        $.getJSON('/ajax/invoices/'+id, function(data){

            var rows;

            for (var i = 0; i < data.length; i++){

                var d = data[i];

                rows += '<tr>';

                rows += '<td><a href="/invoice/view/' + d.id + '">' + d.invoice_number + '</a></td>';

                rows += '<td>$' + d.total + '</td>';

                if (d.paid !== null){
                    rows += '<td>' + d.paid + '</td>';
                }

                else {
                    rows += '<td>' + "unpaid" + '</td>';
                }

                rows += '<td>' + d.due + '</td>';

                rows += '</tr>';

            }

            table.html(rows);

        });

    });

});