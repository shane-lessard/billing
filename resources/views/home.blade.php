@extends('layouts.master')

@section('content')
    <!-- Sidebar for Invoice Settings -->
    <form method="POST" action="/invoice/store">
        <div class="invoice-app">

            <h2>
                <noscript>Please turn on Javascript to use this application.</noscript>
            </h2>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <p><strong>Uh oh</strong></p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="alert alert-success hidden-element">
                <p><strong class="success-message"></strong></p>
            </div>
            <aside class="col-xs-12 col-md-4 col-lg-3">
                <section>
                    <h2>
                        <small>Settings</small>
                    </h2>


                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="form-group">
                        <label>Currency</label>
                        <select name="currency" class="form-control" value="{{Request::old('currency')}}">
                            <option value="usd">USD</option>
                            <option value="cdn" selected>CAD</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Discount</label>

                        <div class="input-group toggle-active-addon">
                            <span class="input-group-addon discount-method active">$</span>
                            <input name="discount" type="number" class="form-control discount"
                                   value="{{Request::old('discount')}}">
                            <input type="hidden" class="discount-method" name="discount_method"
                                   value="{{ Request::old('discount_method') ? Request::old('discount_method') : '$' }}"/>
                            <span class="input-group-addon discount-method" >%</span>

                        </div>
                    </div>
                    <div class="form-group">
                        <label>Tax</label>

                        <div class="input-group toggle-active-addon">
                            <span class="input-group-addon tax-type tax-method">$</span>
                            <input type="number" name="tax" class="form-control tax" value="{{Request::old('tax')}}">
                            <input type="hidden" value="{{ Request::old('tax_method') ? Request::old('tax_method') : '%' }}" class='tax-method' name="tax_method"/>
                            <span class="input-group-addon tax-type tax-method active">%</span>

                        </div>
                    </div>
                    <div class="form-group">
                        <label>Due Date</label>
                        <input type="date" value="{{ $due }}" max="2999-12-31" class="form-control" name="due">
                    </div>
                    <div class="form-group">
                        <label>Invoice Number</label>
                        <input type="number" step='0.01' class="form-control" name="invoice_number"
                               id="invoice_number" value="{{$invoiceNumber}}">
                    </div>
                    <div class="form-group">
                        <label>Notes</label>
                        <textarea name="notes" class="form-control" value="{{ Request::old('notes') }}"></textarea>
                    </div>

                </section>
            </aside>

            <!-- Form - Table for building invoice -->
            <section class="index col-xs-12 col-md-8 col-lg-9">
                <h2>
                    <small>Invoice Builder</small>
                </h2>
                <div class="col-xs-6 col-md-10">
                    <select class="form-control client-list" name="client_id">
                        @if(isset($clients[0]))
                            <option disabled selected value="null">- Select a Client -</option>
                        @else
                            <option disabled selected value="null">- Add a Client to Begin -</option>
                        @endif
                        @foreach($clients as $client)
                            <option value="{{ $client->id }}">{{ $client->name }}
                                &nbsp;&nbsp;&nbsp;&nbsp;&lt;<em>{{ $client->email }}</em>&gt;</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-xs-6 col-md-2">
                    <a class='btn btn-info' href='#' data-toggle="modal" data-target="#new-client">Add New Client</a>
                </div>
                <table class="table table-striped table-responsive invoice-table col-xs-12">
                    <tr class="header">
                        <th>Item Name</th>
                        <th>Item Description</th>
                        <th>Item Cost</th>
                        <th><span class="hidden-xs">Quantity</span><span
                                    class="hidden-md hidden-lg hidden-sm">QTY</span></th>
                        <th>Item Total</th>
                    </tr>
                    <input type="hidden" name="rows" class="rows" value="{{ !empty(old('rows')) ? old('rows') : 1 }}">
                    <!-- TODO Only one empty line at a time (by Line total) -->
                    @for ( $i = 0; $i < $rows; $i++ )
                        <!-- TODO mobile Item Rows need to be accordions that will display Item name -> Total Line Cost as header-->
                    <tr class="item-row">
                        <td>
                            <input type="text" class="form-control form-inline item-name" name="item[]"
                                   placeholder="Item" autocomplete="off" value="{{ old("item")[$i] }}">
                            <ul class="saved-item"></ul>
                        </td>
                        <td>
                            <input type="text" class="form-control form-inline item-description" name="description[]"
                                   placeholder="Description" value="{{ old("description")[$i] }}">
                        </td>
                        <td>
                            <input type="number" step="0.01" class="form-control form-inline item-cost" name="cost[]"
                                   placeholder="100.00" value="{{ old("cost")[$i] }}">
                        </td>
                        <td>
                            <div class="input-group qty-container">
                                <span class="glyphicon glyphicon-chevron-down item-qty show-mobile left"></span>
                                <input type="number" class='form-control form-inline item-qty' min="0" name="quantity[]" value="{{ !empty(old("quantity")[$i]) ? old("quantity")[$i] : 1 }}">
                                <span class="glyphicon glyphicon-chevron-up item-qty show-mobile right"></span>
                                <div class="item-qty-toggles">
                                    <span class="glyphicon glyphicon-chevron-up item-qty hide-xs"></span><br/>
                                    <span class="glyphicon glyphicon-chevron-down item-qty hide-xs"></span>
                                </div>
                            </div>
                        </td>
                        <td>
                            <input type="number" step="0.01" class="form-control form-inline item-total"
                                   name="itemTotal[]" readonly value="{{ old("itemTotal")[$i] }}">
                        </td>
                    </tr>
                @endfor
                </table>
                <div class="col-xs-12 col-sm-8">
                    <div class="btn-group full-mobile">
                        <button class="btn btn-default add-row full-mobile" type="button">New Row</button>
                        <button class="btn btn-default full-mobile" type="submit">Save Invoice</button>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-4">
                    <input class='form-control btn btn-default invoice-total' name='total' type="number" value=""
                           readonly>
                </div>
            </section>

            <!-- Dialog window for new client form -->


        </div>
    </form>
    </div>
    <div class='col-xs-12'>
        <div class="modal fade" id="new-client" tabindex="-1" role="dialog" aria-labelledby="new-client-label" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="new-client-label">New Client</h4>
                    </div>
                    <div class="modal-body">
                        <form role="form" method="POST" action="{{ url('/clients') }}">

                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="form-group">
                                <label class="control-label">Name:</label>
                                <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Email:</label>
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Phone:</label>
                                <input type="text" class="form-control" name="phone" value="{{ old('phone') }}">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Address:</label>
                                <input type="text" class="form-control" name="address" value="{{ old('address') }}">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Business/Title:</label>
                                <input type="text" class="form-control" name="business" value="{{ old('business') }}">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Notes:</label>
                                <textarea class="form-control" rows="5" name="notes" value="{{ old('notes') }}"></textarea>
                            </div>


                            <div class="modal-footer">
                                <div class="form-group">
                                    <button class="btn btn-default btn-info index-new-client" type="submit">Save</button>
                                    <button type="button" class="btn btn-default close-client-form" data-dismiss="modal">Close</button>
                                </div>
                            </div>

                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
@stop
