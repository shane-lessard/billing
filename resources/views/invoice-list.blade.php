@extends('layouts.master')

@section('content')

    <h2>{{ $title }}
        <small>(<a href="/invoice/{{ $type == 'paid' ? 'unpaid' : 'paid' }}">Go to {{ $type == 'paid' ? 'unpaid' : 'paid' }}</a>)</small>
    </h2>
    <table class="table table-striped col-xs-12 invoice-list">
        <tr>
            <th>Invoice Number: </th>
            <th>Client: </th>
            <th>Total: </th>
            <th>{{ $type == "paid" ? "Paid:" : "Due:" }}</th>
            <th>Actions</th>
        </tr>
        @foreach ($invoices as $row)
        <tr>
            <td>{{$row->getAttribute('invoice_number')}}</td>
            <td><a href="/invoice/view/{{$row->id}}">{{$row->client->name}} <span class="hidden-xs">- &lt;{{$row->client->email}}&gt;</span></a></td>
            <td>{{$row->total}}</td>
            <td> {{ $type == "paid" ? $row->paid : $row->due }}</td>
            <td><a href="/invoice/{{ $type == 'paid' ? 'unpay' : 'pay' }}/{{$row->id}}">{{ucfirst( $type == 'paid' ? 'unpay' : 'pay' )}}</a> -
            @if($row->private == 1)
                <a href="/invoice/public/{{$row->id}}">Public</a>
            @else
                <a href="/invoice/private/{{$row->id}}">Private</a>
            @endif
            </td>
        </tr>
        @endforeach
    </table>
    <div class="col-xs-12"><div class="pager"><?php echo $invoices->render(); ?></div></div>
@stop



