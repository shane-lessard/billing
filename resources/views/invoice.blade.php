<!doctype html>
<html>
<head>
    <title>
        Invoice - {{$invoice->created_at->format('Y-m-d')}} - {{$client->name}}
    </title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <!--jQuery-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <!--Bootstrap Styles-->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <!--Bootstrap script-->
    <link href="/css/invoice.css" rel="stylesheet">
    <script src="/js/script.js"></script>

</head>
<body>
@if ( null !== Auth::user() && Auth::user()->id === $invoice->user_id)

    <nav class="navbar navbar-default container-fluid">
        <div class="container">
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="/">Home</a></li>
                @if ($invoice->paid == 0)
                        <li><a href="/invoice/pay/<?= $invoice->id ?>">File as Paid</a></li>
                    @else
                        <li><a href="/invoice/unpay/<?= $invoice->id ?>">File as Unpaid</a></li>
                    @endif
                    @if ($invoice->private == 0)
                        <li><a href="/invoice/private/<?= $invoice->id ?>">Make Private</a></li>
                    @else
                        <li><a href="/invoice/public/<?= $invoice->id ?>">Make Public</a></li>
                    @endif
                    <li><a href="/invoice/pdf/{{ $invoice->id }}">Save as PDF</a></li>

                    @if ($invoice->paid !== null)

                        <li><a href="/invoice/receipt/{{ $invoice->id }}">Download Receipt</a></li>

                    @endif

                    <li><form action="/invoice/remove/{{$invoice->id}}"><button type="submit" class="btn btn-link remove-invoice">Delete Invoice</button></form></li>

                </ul>
            </div>
        </div>
    </nav>
@else
    <nav class="navbar navbar-default container-fluid">
        <div class="container">
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="/invoice/pdf/{{ $invoice->id }}">Save as PDF</a></li>
                    <li><a href="/">Home</a></li>
                </ul>
            </div>
        </div>
    </nav>
@endif

<div class="container invoice-container">

    <div class="col-xs-8  col-xs-offset-2">
        <div class="col-xs-8">
            <h1>Invoice</h1>
        </div>
        <div class="col-xs-4">
            <table class="borderless" >
                <tr>
                    <th>
                        {{"From: " . $biller->name}}
                    </th>
                </tr>
                @if($biller->business != "")
                    <tr>
                        <td>
                            {{$biller->business}}
                        </td>
                    </tr>
                @endif
                <tr>
                    <td>
                        {{$biller->email}}
                    </td>
                </tr>
                <tr>
                    <td>
                        {{$biller->address_line_1}}
                    </td>
                </tr>
                <tr>
                    <td>
                        {{$biller->address_line_2}}{{ $biller->address_line_2 != "" && $biller->postal != "" ? ', ' : '' }}{{$biller->postal}}
                    </td>
                </tr>
                <tr>
                    <td>
                        {{$biller->city}}{{$biller->city != "" && $biller->province != "" ? ", " : "" }}{{$biller->province}}{{$biller->province != "" || $biller->city != "" ? ", " : ""}}{{$biller->country}}
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-xs-12" style="height:60px;"></div>
        <div class="col-xs-12">
            <table class="col-xs-8 borderless" >
                <tr>
                    <th>Billing Address</th>
                </tr>
                <tr>
                    <td>
                        {{$client->name}}
                    </td>
                </tr>
                @if ($client->business)
                    <tr>
                        <td>
                            {{$client->business}}
                        </td>
                    </tr>
                @endif
                <tr>
                    <td>
                        {{$client->email}}
                    </td>
                </tr>
                <tr>
                    <td>
                        {{$client->address}}
                    </td>
                </tr>
            </table>

            <table class="col-xs-4" >
                <tr>
                    <td>
                        Invoice Number:
                    </td>
                    <td>
                        {{$invoice->getAttribute('invoice_number')}}
                    </td>
                </tr>
                <tr>
                    <td>
                        Date:
                    </td>
                    <td>
                        {{$invoice->created_at->format('Y-m-d') }}
                    </td>
                </tr>
                <tr>
                    <td>
                        Due:
                    </td>
                    <td>
                        {{$invoice->due}}
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-xs-12" style="height:35px;"></div>
        <div class="col-xs-12">
            <table class="table table-striped" >
                <tr>
                    <th>Item Name:</th>
                    <th>Description:</th>
                    <th>Cost:</th>
                    <th>Quantity</th>
                    <th>Line Total:</th>
                </tr>
                @foreach($rows as $row)
                    <tr>
                        <td>{{$row->item}}</td>
                        <td>{{$row->description}}</td>
                        <td>{{$row->cost}}</td>
                        <td>{{$row->quantity}}</td>
                        <td>{{$row->total}}</td>
                    </tr>
                @endforeach
            </table>
        </div>

        <div class="col-xs-8">

        </div>
        <div class="col-xs-4">
            <table class="table table-striped">
                @if($invoice->tax)
                <tr>
                    <td>Tax:</td>
                    <td>{{$invoice->getAttribute('tax_method')}} {{$invoice->tax}}</td>
                </tr>
                @endif
                @if($invoice->discount)
                <tr>
                    <td>Discount:</td>
                    <td>{{$invoice->getAttribute('discount_method')}} {{$invoice->discount}}</td>
                </tr>
                @endif
                <tr>
                    <td>Total:</td>
                    <td>$ {{$invoice->total}} {{strtoupper($invoice->currency)}}</td>
                </tr>
            </table>
        </div>
    @if ($invoice->notes != "")
        <div class="col-xs-12">
            <div class="invoice-footer panel panel-default">
                <div class="panel-body">
                    {!!nl2br($invoice->notes)!!}
                </div>
            </div>
        </div>
    @endif

    </div>
</div>
</body>
</html>