<div class='col-xs-0 col-md-4' >




</div>

<div class='col-xs-12 col-md-4 text-center' >
    @if(ENV("COPY") != "")
    <p>&copy;<?= Date('Y') ?> Shane Lessard</p>
    @endif
</div>

<div class='col-xs-12 col-md-4 text-right' >

    @if (Auth::check())


        <p><a href='/account'>Account</a> - <a href='/auth/logout'>Logout</a></p>

    @else

        <p><a href='/auth/login'>Log In</a> or <a href='/auth/register'>Register</a></p>

    @endif

</div>