<!DOCTYPE html>
<html>
    <head>
        <title>
                @if (isset($title))
                    {{ $title }}
                @else
                Shane Lessard - | Professional Invoice Management
                @endif

        </title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script><!--jQuery-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"><!--Bootstrap Styles-->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script><!--Bootstrap script-->
        <link href='http://fonts.googleapis.com/css?family=Arvo:400,700,400italic|Ubuntu:300,400,700,400italic|Titillium+Web:400,700,200,400italic|Roboto:400,700,200' rel='stylesheet' type='text/css'><!--google font-->


    </head>
    <body>
        <header class="container-fluid">
            <div class="container">
                @include('layouts.nav')
            </div>
        </header>
        <section class="container-fluid">
            <article class="container main-container">
                @yield('content')
            </article>
        </section>
        <footer class="container-fluid">
            <div class="container">
                @include('layouts.footer')
            </div>
        </footer>
        <link rel="stylesheet" href="/css/style.css" /><!-- Custom styles-->
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.16/angular.min.js"></script><!--AngularJS -->
        <script src="/js/script.js">//Master Script file</script>


        @if(getenv('APP_ENV') == "production")
            <script>
                (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

                ga('create', 'UA-52493601-6', 'auto');
                ga('send', 'pageview');

            </script>
        @endif
    </body>
</html>