<nav class="navbar navbar-default" role="navigation">

                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/">Shane Lessard <small>Accounts</small></a>
                </div>
                @if(Auth::check())
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-right" id="navbar-collapse">
                    <ul class="nav navbar-nav">

                        <li><a href="/account">Profile</a></li>
                        <li><a href="/clients">Clients</a></li>

                        @if(App\User::hasInvoice())
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Invoices <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">

                                @if(App\User::hasUnpaid())
                                <li><a href="/invoice/unpaid">Unpaid Invoices</a></li>
                                @endif
                                @if(App\User::hasPaid())
                                <li><a href="/invoice/paid">Paid Invoices</a></li>
                                @endif

                            </ul>
                        </li>
                        @endif
                    </ul>
                </div><!-- /.navbar-collapse -->
                @endif
        </nav>



