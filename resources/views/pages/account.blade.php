@extends('layouts.master')

@section('content')
    <div class="col-xs-12 col-md-8 col-md-offset-2">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <h2>User Settings</h2>
        <form class="form-horizontal" role="form" method="POST" action="{{ url('/user/update') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="id" value="{{ Auth::user()->id }}">
            <table class="table table-striped col-xs-12">
                <tr>
                    <td><h4>User Details</h4></td>
                    <td><button type="submit" class="btn btn-default btn-info form-control" name="changeInfoSubmit">Save</button></td>
                </tr>
                <tr>
                    <td>Name</td>
                    <td><input type='text' name='name' value='{{ $user['name'] }}' class='form-control'></td>
                </tr>
                <tr>
                    <td>Email Address</td>
                    <td><input type='text' name='email' value='{{ $user['email'] }}' class='form-control'></td>
                </tr>
                <tr>
                    <td>Business / Title</td>
                    <td><input type='text' name='business' value='{{ $user['business'] }}' class='form-control'></td>
                </tr>
                <tr>
                    <td>Address Line 1</td>
                    <td><input type='text' name='address_line_1' value='{{ $user['address_line_1'] }}' class='form-control'></td>
                </tr>
                <tr>
                    <td>Address Line 2</td>
                    <td><input type='text' name='address_line_2' value='{{ $user['address_line_2'] }}' class='form-control'></td>
                </tr>
                <tr>
                    <td>Postal/Zip code</td>
                    <td><input type='text' name='postal' value='{{ $user['postal'] }}' class='form-control'></td>
                </tr>
                <tr>
                    <td>City</td>
                    <td><input type='text' name='city' value='{{ $user['city'] }}' class='form-control'></td>
                </tr>
                <tr>
                    <td>Province/State</td>
                    <td><input type='text' name='province' value='{{ $user['province'] }}' class='form-control'></td>
                </tr>
                <tr>
                    <td>Country</td>
                    <td><input type='text' name='country' value='{{ $user['country'] }}' class='form-control'></td>
                </tr>

            </table>
        </form>
       </div>

@stop
