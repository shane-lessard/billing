@extends('layouts.master')

@section('content')

    <div class="col-xs-12">

        <ul>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <p></p><strong>Uh oh,</strong> looks like you done screwed up the form somehow. </p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </ul>

        <div class="panel panel-default">
            <div class="panel-heading clearfix">
                <span>Clients</span><a class='pull-right' href='#' data-toggle="modal" data-target="#new-client">Add New Client</a>
            </div>
            <div class="'panel-body">
                <table class="table client-table table-striped col-xs-12">
                    <thead>
                        <tr>
                            <th>Name <small class="hidden-xs">(Click for details)</small></th>
                            <th class="hidden-xs">Email Address</th>
                            <th class="hidden-xs">Business/Title</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $row_id = 0 //Count rows for classes/IDs?>
                        @foreach($clients as $client)
                        <?php $row_id++ ?>
                        <tr>
                            <td><a href='#' data-toggle="modal" data-target="#dialog{{ $row_id }}">{{ $client->name  }}</a></td>
                            <td class="hidden-xs">{{ $client->email }}</td>
                            <td class="hidden-xs">{{ $client->business }}</td>
                            <td>
                                <!-- Get Clients List Ajax -->
                                <a href='#' data-toggle="modal" data-target="#client-invoices" class="inline-block btn btn-info btn-xs get-list" data-cid="{{ $client->id }}" data-name="{{  $client->name }}">Get Invoices</a>
                                <!-- Delete button -->
                                <form class="inline-block" role="form" method="POST" action="{{ url('/clients/'.$client->id) }}">
                                    <input name="_method" type="hidden" value="DELETE">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="id" value="{{ $client->id }}">
                                    <button type="submit" class="btn btn-info btn-xs remove-client">Delete</button>
                                </form>

                            </td>
                        </tr>

                            <!-- Dialoge Window Markup -->
                        <div class="modal fade" id="dialog{{ $row_id }}" role="dialog" aria-labelledby="dialogueLabel{{ $row_id }}" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="dialogueLabel{{ $row_id }}">{{ $client->name }}</h4>
                                    </div>
                                    <div class="modal-body">

                                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/clients/'.$client->id) }}">
                                            <input name="_method" type="hidden" value="PUT">

                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="hidden" name="id" value="{{ $client->id }}">
                                            <input type="hidden" name="name" value="{{ $client->name }}">

                                            <div class="form-group">
                                                <label class="control-label">Address:</label>
                                                <input type="text" class="form-control" name="address" value="{{ $client->address }}">
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Phone:</label>
                                                <input type="text" class="form-control" name="phone" value="{{ $client->phone }}">
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Business:</label>
                                                <input type="text" class="form-control" name="business" value="{{ $client->business }}">
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label">Email:</label>
                                                <input type="email" class="form-control" name="email" value="{{ $client->email }}">
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Notes:</label>
                                                <textarea class="form-control" name="notes">{{ $client->notes }}</textarea>
                                            </div>


                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-info get-invoices">Save Changes</button>
                                        </form>
                                        <form class="inline-block" role="form" method="POST" action="{{ url('/clients/'.$client->id) }}">
                                            <input name="_method" type="hidden" value="DELETE">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="hidden" name="id" value="{{ $client->id }}">
                                        <button type="submit" class="btn btn-info remove-client">Delete</button>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div>

                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="col-xs-12"><div class="pager"><?php echo $clients->render(); ?></div></div>

    </div>


    <!-- Dialog for new client form -->
    <div class='col-xs-12'>
        <div class="modal fade" id="new-client" tabindex="-1" role="dialog" aria-labelledby="new-client-label" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="new-client-label">New Client</h4>
                    </div>
                    <div class="modal-body">
                        <form role="form" method="POST" action="{{ url('/clients') }}">

                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="form-group">
                                <label class="control-label">Name:</label>
                                <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Email:</label>
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Phone:</label>
                                <input type="text" class="form-control" name="phone" value="{{ old('phone') }}">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Address:</label>
                                <input type="text" class="form-control" name="address" value="{{ old('address') }}">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Business/Title:</label>
                                <input type="text" class="form-control" name="business" value="{{ old('business') }}">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Notes:</label>
                                <textarea class="form-control" rows="5" name="notes" value="{{ old('notes') }}"></textarea>
                            </div>

                            <div class="modal-footer">
                                <div class="form-group">
                                    <button class="btn btn-default btn-info" type="submit">Save</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>

                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <!-- Client Invoices List -->

    <div class='col-xs-12'>
        <div class="modal fade" id="client-invoices" tabindex="-1" role="dialog-invoice" aria-labelledby="client-invoices" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="client-invoices"></h4>
                    </div>
                    <div class="modal-body">

                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Invoice Number</th>
                                    <th>Total</th>
                                    <th>Paid</th>
                                    <th>Due</th>
                                </tr>

                            <!-- Populated with javascript -->
                            </thead>
                            <tbody>
                            </tbody>

                        </table>

                        <div class="modal-footer">
                            <div class="form-group">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

@stop