<!doctype html>
<html>
<head>
    <title>
        Invoice - {{$invoice->created_at->format('Y-m-d')}} - {{$client->name}}
    </title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

    <style>
        .borderless tbody tr td, .borderless tbody tr th, .borderless thead tr th {
            border: none;
        }

        h1 {
            font-size:5em;
            font-weight:100;
        }

        table {
            border-spacing: 2px;
        }

        th, td {
            padding: 3px;
        }

    </style>

</head>
<body>

<div class="container invoice-container">

    <div class="col-xs-12">
            <div class="col-xs-8">
                <h1>Invoice</h1>
            </div>
            <div class="col-xs-4">
                <table class="borderless" >
                    <tr>
                        <th>
                            {{"From: " . $biller->name}}
                        </th>
                    </tr>
                    @if($biller->business != "")
                        <tr>
                            <td>
                                {{$biller->business}}
                            </td>
                        </tr>
                    @endif
                    <tr>
                        <td>
                            {{$biller->email}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{$biller->address_line_1}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{$biller->address_line_2}}{{ $biller->address_line_2 != "" && $biller->postal != "" ? ', ' : '' }}{{$biller->postal}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{$biller->city}}{{$biller->city != "" && $biller->province != "" ? ", " : "" }}{{$biller->province}}{{$biller->province != "" || $biller->city != "" ? ", " : ""}}{{$biller->country}}
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-xs-12" style="height:60px;"></div>
            <div class="col-xs-12">
                <table class="col-xs-8 borderless" >
                    <tr>
                        <th>Billing Address</th>
                    </tr>
                    <tr>
                        <td>
                            {{$client->name}}
                        </td>
                    </tr>
                    @if ($client->business)
                        <tr>
                            <td>
                                {{$client->business}}
                            </td>
                        </tr>
                    @endif
                    <tr>
                        <td>
                            {{$client->email}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{$client->address}}
                        </td>
                    </tr>
                </table>

                <table class="col-xs-4" >
                    <tr>
                        <td>
                            Invoice Number:
                        </td>
                        <td>
                            {{$invoice->getAttribute('invoice_number')}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Date:
                        </td>
                        <td>
                            {{$invoice->created_at->format('Y-m-d') }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Due:
                        </td>
                        <td>
                            {{$invoice->due}}
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-xs-12" style="height:35px;"></div>
            <div class="col-xs-12">
                <table class="table table-striped" >
                    <tr>
                        <th>Item Name</th>
                        <th>Description</th>
                        <th>Cost</th>
                        <th>Quantity</th>
                        <th>Line Total</th>
                    </tr>
                    @foreach($rows as $row)
                        <tr>
                            <td>{{$row->item}}</td>
                            <td>{{$row->description}}</td>
                            <td>{{$row->cost}}</td>
                            <td>{{$row->quantity}}</td>
                            <td>{{$row->total}}</td>
                        </tr>
                    @endforeach
                </table>
            </div>

            <div class="col-xs-8">

            </div>
            <div class="col-xs-4">
                <table class="table table-striped" style="margin-top:-20px;">
                    @if($invoice->tax)
                        <tr>
                            <td>Tax:</td>
                            <td>{{$invoice->getAttribute('tax_method')}} {{$invoice->tax}}</td>
                        </tr>
                    @endif
                    @if($invoice->discount)
                        <tr>
                            <td>Discount:</td>
                            <td>{{$invoice->getAttribute('discount_method')}} {{$invoice->discount}}</td>
                        </tr>
                    @endif
                    <tr>
                        <td>Total:</td>
                        <td>$ {{$invoice->total}} {{strtoupper($invoice->currency)}}</td>
                    </tr>
                </table>
            </div>
        @if ($invoice->notes != "")
            <div class="col-xs-12">
                <div class="invoice-footer panel panel-default">
                    <div class="panel-body">
                        {!!nl2br($invoice->notes)!!}
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>
</body>
</html>