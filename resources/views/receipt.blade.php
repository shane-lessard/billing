<!doctype html>
<html>
<head>
    <title>
        Invoice - {{$invoice->created_at->format('Y-m-d')}} - {{$client->name}}
    </title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

    <style>
        .borderless tbody tr td, .borderless tbody tr th, .borderless thead tr th {
            border: none;
        }

        h1 {
            font-size:5em;
            font-weight:100;
            text-align:center;
        }

        table {
            border-spacing: 2px;
        }

        th, td {
            padding: 3px;
        }

        td.filled {
            background: #dddddd;
        }

        .space-top {
            margin-top:25px;
        }

        .space-bottom {
            margin-bottom:25px;
        }

    </style>

</head>
<body>
<div class="container">
    <div class="col-xs-6 col-xs-offset-3">

        <h1>Receipt</h1>

        <table class="table space-top" >
            <tr>
                <th>Item Name</th>
                <th>Quantity</th>
                <th>Line Total</th>
            </tr>
            @foreach($rows as $row)
                <tr>
                    <td>{{$row->item}}</td>
                    <td>{{$row->quantity}}</td>
                    <td>{{$row->total}}</td>
                </tr>
            @endforeach
            @if($invoice->tax)
                <tr>
                    <td class="filled"></td>
                    <th>Tax:</th>
                    <td>{{$invoice->getAttribute('tax_method')}} {{$invoice->tax}}</td>
                </tr>
            @endif
            @if($invoice->discount)
                <tr>
                    <td class="filled"></td>
                    <th>Discount:</th>
                    <td>{{$invoice->getAttribute('discount_method')}} {{$invoice->discount}}</td>
                </tr>
            @endif
            <tr>
                <td class="filled"></td>
                <th>Total:</th>
                <td>$ {{$invoice->total}} {{strtoupper($invoice->currency)}}</td>
            </tr>
        </table>

        <table class="table space-top">
            <tr>
                <th>
                    Invoice Number:
                </th>
                <td>
                    {{$invoice->getAttribute('invoice_number')}}
                </td>
            </tr>
            <tr>
                <th>
                    Invoice Date:
                </th>
                <td>
                    {{$invoice->created_at->format('Y-m-d') }}
                </td>
            </tr>
            <tr>
                <th>
                    Paid:
                </th>
                <td>{{ date('Y-m-d',strtotime($invoice->paid)) }}</td>
            </tr>
        </table>
        <div class="col-xs-6">
        <table class="table space-top">
            <tr>
                <th>
                   Paid By:
                </th>
            </tr>
            <tr>
                <td>
                    {{$client->name}}
                </td>
            </tr>
            @if ($client->business)
                <tr>
                    <td>
                        {{$client->business}}
                    </td>
                </tr>
            @endif
            </table>
            </div>
            <div class="col-xs-6">
            <table class="table space-top">
            <tr>
                <th>Paid To:</th>
            </tr>
            <tr>
                <td>
                    {{$biller->name}}
                </td>
            </tr>
            @if($biller->business != "")
                <tr>
                    <td>
                        {{$biller->business}}
                    </td>
                </tr>
            @endif
        </table>
        </div>
    </div>
</div>
</body>
</html>